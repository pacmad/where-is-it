<?php

function floor_form( $form, &$form_state, $floor = NULL, $op = 'edit', $entity_type = NULL  ) {

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($floor->label) ? $floor->label : '',
  );

  $form['code'] = array(
    '#title' => t('Code'),
    '#type' => 'textfield',
    '#default_value' => isset($floor->code) ? $floor->code : '',
  );

  $form['image'] = array(
    '#title' => t('Floor image'),
    '#description' => t('Upload a file to use as the map background image for this floor.'),
    '#type' => 'file',
  );

  $form['weight'] = array(
    '#title' => t('Weight'),
    '#type' => 'weight', 
    '#default_value' => isset($floor->weight) ? $floor->weight : 0,
    '#description' => t('In lists, the heavier items will sink and the lighter items will be positioned nearer the top.'),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Floor'),
    '#weight' => 40,
  );

  return $form;
}

function floor_form_validate( &$form, &$form_state ) {
  $validators = array(
    'file_validate_is_image' => array('image'),
  );

  /**
   * Some logic here to do the image processing and validation. 
   * - If there is a file included in the form submission, process the file and validate that it is an image.
   * - If it's a valid image, set the image path, width and height properties accordingly.
   * - If there isn't a file included, use existing values if there are some. If not, throw a validation error. 
   */
  $file = file_save_upload('image', array(), 'public://ug_whereisit/', FILE_EXISTS_REPLACE);
  if( !empty($file) ) {
    $file->status = FILE_STATUS_PERMANENT;
    $file = file_save($file);
    $form_state['values']['image'] = $file->uri;
    $form_state['values']['image_width'] = $file->image_dimensions['width'];
    $form_state['values']['image_height'] = $file->image_dimensions['height'];
  }
  else {
    if( isset($form_state['floor']) && !empty($form_state['floor']->image) ) {
      $old_image = $form_state['floor']->image;
      $form_state['values']['image'] = $old_image;
    }
    else {
      form_set_error('image', t('Please upload a floor map image.'));
    }
  }
}

function floor_form_submit( &$form, &$form_state ) {
  $floor = entity_ui_form_submit_build_entity($form, $form_state);
  $floor->save();
  $form_state['redirect'] = 'admin/config/whereisit/floors';
}

