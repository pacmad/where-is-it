<?php

function area_form ( $form, &$form_state, $area = NULL, $op = 'edit', $entity_type = NULL  ) {
  drupal_add_js(array('ug_whereisit' => array(
    'box_colour' => variable_get('ug_whereisit_box_colour', '000000'),
    'line_width' => variable_get('ug_whereisit_line_width', 3), 
    'fill_transparency' => variable_get('ug_whereisit_fill_transparency', 0.7), 
  )), array('type' => 'setting'));
  drupal_add_js(drupal_get_path('module', 'ug_whereisit') . '/js/map.js');
  drupal_add_js(drupal_get_path('module', 'ug_whereisit') . '/js/area.js');
  drupal_add_css(drupal_get_path('module', 'ug_whereisit') . '/css/area.css');
  
  //jQuery UI dialog plugin is not included by default.
  drupal_add_library('system', 'ui.dialog');
 
  $form['label'] = array(
    '#title' => t('Label'),
    '#description' => t('The public facing label for this area.'),
    '#type' => 'textfield',
    '#default_value' => isset($area->label) ? $area->label : '',
    '#maxlength' => '255',
    '#required' => TRUE,
  );

  $form['code'] = array(
    '#title' => t('Code'),
    '#description' => t('The text value used to identify this area in the URL. Leave blank to automatically generate a code.'),
    '#type' => 'textfield',
    '#default_value' => isset($area->code) ? $area->code : '',
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#description' => t('Provide a text based description of the general location of the area. This text is used as an alternate text representation of the map image.'),
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => isset($area->description) ? $area->description : '',
  );

  $form['floor'] = array(
    '#title' => t('Floor'),
    '#type' => 'select',
    //TODO: Replace this with floor entity.
    '#options' => ug_whereisit_get_entity_select(ug_whereisit_get_floors()),
    '#default_value' => isset($area->floor) ? $area->floor : '',
  );

  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    //TODO: Replace this with floor entity.
    '#options' => array(
      'named' => t('Named'),
      'call' => t('Call Number'),
    ),
    '#default_value' => isset($area->type) ? $area->type : '',
  );

  $form['call_number'] = array(
    '#type' => 'fieldset',
    '#title' => t('Call Number'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#attributes' => array(
      'id' => array('area-call-number'),
    ),
  );
 
  $form['call_number']['resource_type'] = array(
    '#title' => t('Material type'),
    '#type' => 'select',
    '#options' => ug_whereisit_get_entity_select(ug_whereisit_get_material_types()),
    '#default_value' => isset($area->resource_type) ? $area->resource_type : '',
  );

  $form['call_number']['start'] = array(
    '#title' => t('Call Number Start'),
    '#description' => "The start of the call number range.",
    '#type' => 'textfield',
    '#default_value' => isset($area->start) ? $area->start : '',
    '#maxlength' => '64', 
    '#required' => FALSE,
  );

  $form['call_number']['end'] = array(
    '#title' => t('Call Number End'),
    '#description' => "The end of the call number range.",
    '#type' => 'textfield',
    '#default_value' => isset($area->end) ? $area->end : '', 
    '#maxlength' => '64',
    '#required' => FALSE,
  );



  /**
   * Logic for rendering and populating the bound fieldsets.
   * -First check if we're editing an area ($area is not null). 
   * -Then load bounds associated with the area and update the value of $form_state['num_bounds'] if it doesn't exist, 
   *  which should only happen on the first form load and not after adding additional bounds to the area.  
   */
  $bounds = array();
  if(!is_null($area) && !$area->is_new) {
    $bounds = ug_whereisit_get_bounds($area->id);
    $bounds = ug_whereisit_fix_bounds_index($bounds);
    if ( !isset($form_state['num_bounds']) ) {
      $form_state['num_bounds'] = sizeof($bounds);
    }
  } 

  //If it's a new area, or no bounds are defined load the form with a single bound fieldset.
  if( !isset($form_state['num_bounds']) ) {
    $form_state['num_bounds'] = 1;
  }

  //Loop through and add the bound fieldsets to the form.
  for( $i = 1; $i <= $form_state['num_bounds']; $i++ ) {

    //This skip elements which have been marked as "Removed"
    if (in_array($i, $form_state['deleted_bounds'])) continue;

    $form['bound'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Box #@num', array('@num' => $i)),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => array(
        'index' => $i,
        'class' => array('box'),
      ),
    );

    $form['bound'][$i]["x-$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Left'),
      '#size' => 10,
      '#default_value' => ug_whereisit_bound_get_default($bounds, $i, 'x'),
    );

    $form['bound'][$i]["y-$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Top'),
      '#size' => 10,
      '#default_value' => ug_whereisit_bound_get_default($bounds, $i, 'y'),
    );

    $form['bound'][$i]["width-$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#size' => 10,
      '#default_value' => ug_whereisit_bound_get_default($bounds, $i, 'width'),
    );

    $form['bound'][$i]["height-$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#size' => 10,
      '#default_value' => ug_whereisit_bound_get_default($bounds, $i, 'height'),
    );

    $form['bound'][$i]["remove-$i"] = array(
      '#type' => 'submit',
      '#value' => 'Remove box',
      '#submit' => array('ug_whereisit_remove_box_callback'),
      '#name' => "remove-box-$i",
      '#limit_validation_errors' => array(),
    );
  }

  $form['map'] = array(
    '#prefix' => '<div class="" style="display: none;" id="map-wrapper">',
    '#markup' => '<canvas id="map"></canvas>',
    '#suffix' => '</div>',
  );

  $form['add_bound'] = array(
    '#type' => 'submit',
    '#value' => t('Add another box'),
    '#submit' => array('ug_whereisit_add_bound'),
    '#limit_validation_errors' => array(),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Area'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Validation handler for area form.
 */
function area_form_validate( &$form, &$form_state ) {
  $type = $form_state['values']['type'];
    
  if($type == 'call') {
    if( empty($form_state['values']['start']) ) {
      form_set_error('start', t('A starting call number is required for call number areas.'));
    }

    if( empty($form_state['values']['end']) ) {
      form_set_error('end', t('An ending call number is required for call number areas.'));
    }
  }
}

/**
 * Submit handler for area form.
 */
function area_form_submit( &$form, &$form_state ) {
  //First build the bounds objects, which we'll later update with the ID from the area object.
  $bounds = ug_whereisit_make_bounds($form_state); 

  //Remove bounds which aren't valid.
  ug_whereisit_remove_empty_bounds( $bounds );

  //Remove the bound fields from the form arrays before we build and save the area.
  ug_whereisit_form_remove_bounds($form, $form_state);

  //If code is blank, automatically generate it using the label. Otherwise, make sure it's good to use in a URL.
  if( empty($form_state['values']['code']) ) {
    $form_state['values']['code'] = ug_whereisit_slug($form_state['values']['label']);
  }
  else {
    $form_state['values']['code'] = ug_whereisit_slug($form_state['values']['code']);
  } 
  
  //Build and save area.
  $area = entity_ui_form_submit_build_entity($form, $form_state);
  $area->save();

  //Delete existing bounds before adding the new bounds.
  ug_whereisit_delete_bounds($area->id);

  //Now go through and add the area ids to the bounds and save each one.
  foreach($bounds as $bound) {
    $bound->aid = $area->id;
    $bound->save();
  }
  
  $form_state['redirect'] = 'admin/config/whereisit/areas';
  
}

/**
 * Submit callback for remove box button.
 */
function ug_whereisit_remove_box_callback ($form, &$form_state) {
  $button_id = $form_state['triggering_element']['#id'];
  $index = substr($button_id, strrpos($button_id, '-') + 1);

  if(!isset($form_state['deleted_bounds'])) {
    $form_state['deleted_bounds'] = array();
  }
  $form_state['deleted_bounds'][] = $index;

  $form_state['rebuild'] = TRUE;
}

/**
 * Given the form state, create an array of bound objects.
 */
function ug_whereisit_make_bounds($form_state) {
  $bounds = array();
  for($i = 1; $i <= $form_state['num_bounds']; $i++) {
    $b = $form_state['complete form']['bound'][$i];

    $bound = entity_create('bound', array());
    $bound->x = $b["x-$i"]['#value'];
    $bound->y = $b["y-$i"]['#value'];
    $bound->height = $b["height-$i"]['#value'];
    $bound->width = $b["width-$i"]['#value'];
    $bounds[] = $bound;
  }
  return $bounds;
}

/**
 * Remove invalid bounds - Bounds will all zero or empty values.
 */
function ug_whereisit_remove_empty_bounds( &$bounds ) {
  foreach( $bounds as $key => $bound ) {
    if( empty($bound->x) && empty($bound->y) && empty($bound->width) && empty($bound->height) ) {
      unset($bounds[$key]);
    }
  }
}

/**
 * Remove bound form elements from the form.
 */
function ug_whereisit_form_remove_bounds( &$form, &$form_state ) {
  unset($form_state['complete form']['bound']);
  unset($form['bound']);

  for($index=1; $index <= $form_state['num_bounds']; $index++) {
    unset($form_state['values']["x-$index"]);
    unset($form_state['values']["y-$index"]);
    unset($form_state['values']["height-$index"]);
    unset($form_state['values']["width-$index"]);

    unset($form_state['input']["x-$index"]);
    unset($form_state['input']["y-$index"]);
    unset($form_state['input']["height-$index"]);
    unset($form_state['input']["width-$index"]);
  }
}

/**
 * Submit handler for the 'Add another box' button.
 */
function ug_whereisit_add_bound($form, &$form_state) {
  // Everything in $form_state is persistent, so we'll just use
  // $form_state['add_name']
  $form_state['num_bounds']++;

  // Setting $form_state['rebuild'] = TRUE causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

/**
 * Helper function to get the default value for a bound field in the area form.
 */
function ug_whereisit_bound_get_default( $bounds, $index, $field ) {
  //First check to see if we have a corresponding bound for this index.
  if( !isset($bounds[$index]) ) {
    return '';
  }

  //If we have a bound entity, check to see if there is a corresponding property in the entity. 
  $bound = $bounds[$index];
  if( isset($bound->$field) ) {
    return $bound->$field;
  }
 
  return '';
}

/**
 * entity_load indexes the arrays of entities using the entity ID, but we need to start indexing at 1 to match the form.
 * This function reindexes the array starting at 1.
 */
function ug_whereisit_fix_bounds_index($bounds) {
  $output = array();
  $size = sizeof($bounds);
  for($i = 0; $i < $size; $i++) {
    $output[$i+1] = array_shift($bounds);
  }
  return $output;
}
