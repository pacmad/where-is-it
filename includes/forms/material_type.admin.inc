<?php

function material_type_form ( $form, &$form_state, $type = NULL, $op = 'edit', $entity_type = NULL  ) {

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($type->label) ? $type->label : '',
  );

  $form['code'] = array(
    '#title' => t('Code'),
    '#type' => 'textfield',
    '#default_value' => isset($type->code) ? $type->code : '',
  );

  $form['weight'] = array(
    '#title' => t('Weight'),
    '#type' => 'weight', 
    '#default_value' => isset($type->weight) ? $type->weight : 0,
    '#description' => t('In lists, the heavier items will sink and the lighter items will be positioned nearer the top.'),
  );

  $form['live'] = array(
     '#type' => 'select',
     '#title' => t('Live'),
     '#options' => array(
        0 => t('No'),
        1 => t('Yes'),
     ),
     '#default_value' => isset($type->live) ? $type->live : 0,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Material Type'),
    '#weight' => 40,
  );

  return $form;
}

function material_type_form_submit ( &$form, &$form_state ) {
  $type = entity_ui_form_submit_build_entity($form, $form_state);
  $type->save();
  $form_state['redirect'] = 'admin/config/whereisit/types';
}
