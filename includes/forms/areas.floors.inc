<?php

function ug_whereisit_named_floors_form() {
  $form = array();
  $floors = ug_whereisit_get_floors();

  $warning = '<span class="element-invisible">Note that changing the value of this element will cause the page to reload.</span>';
  foreach( $floors as $floor ) {
    $form['floors'][$floor->code] = array(
      '#type' => 'markup',
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );

    /**
     * This nesting is required to support the JS that does the redirects.
     * See js/where.js
     */

    $label = $floor->label;

    $form['floors'][$floor->code]['select'] = array(
      '#type' => 'select',
      '#title' => t($label) . $warning,
      '#options' => ug_whereisit_select_add_default(
        'Choose...',
        function() use ($floor) {
          return ug_whereisit_get_named_areas_select($floor->code);
        }
      ),
    );

    $form['floors'][$floor->code]['submit'] = array(
      '#type' => 'submit',
      '#value' => t("$label: Go to location"),
      '#attributes' => array(
        'class' => array('where-floor-submit', 'element-invisible'),
      ),  
    );
  }

  return $form; 
}

