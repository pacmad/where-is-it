<?php

/**
 * Floor class
 */
class Floor extends Entity {

  public function uri() {
    return array ('path' => 'admin/config/whereisit/floors/floor/' . $this->id);
  }

}
