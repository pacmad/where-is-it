<?php

/**
 * Area class
 */
class Area extends Entity {

  public function uri() {
    return array ('path' => 'admin/config/whereisit/areas/area/' . $this->id);
  }

}
