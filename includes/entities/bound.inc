<?php

/**
 * Bound class
 */
class Bound extends Entity {

  public function uri() {
    return array ('path' => 'admin/config/whereisit/bounds/bound/' . $this->id);
  }

}
