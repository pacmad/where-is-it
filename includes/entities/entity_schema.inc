<?php

function ug_whereisit_get_entity_schema() {
  $schema = array();

  $status_cols = ug_whereisit_entity_exportable_schema_fields();

  $schema['ug_whereisit_areas'] = array(
    'description' => 'Base table for areas.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for an area.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The title of this area, always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of this area.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
      'floor' => array(
        'description' => 'The floor where this area resides.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'type' => array(
        'description' => 'The type of the area.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => 'named',
      ),
      'code' => array(
        'description' => 'The code for a named area.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
      ),
      'resource_type' => array(
        'description' => 'The type of resource stored in this area.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
        'default' => '',
      ),
      'start' => array(
        'description' => 'The starting call number for this area.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
        'default' => '',
      ),
      'end' => array(
        'description' => 'The ending call number for this area.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
        'default' => '',
      ),
      'module' => $status_cols['module'],
      'status' => $status_cols['status'],
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'id' => array('id'),
      'floor' => array('floor'),
      'type' => array('type'),
      'resource_type' => array('resource_type'),
    ),
    'foreign keys' => array(
      'floor' => array(
        'table' => 'ug_whereisit_floors',
        'columns' => array('floor' => 'id'),
      ),
    ),

  );
  
  $schema['ug_whereisit_material_types'] = array(
    'description' => 'Base table for material types.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for an area.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'code' => array(
        'description' => 'The internal code string for this material type.',
        'type' => 'varchar', 
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'label' => array(
        'description' => 'The title of this resource type, always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'description' => 'The order in which this resource should appear in lists..',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'live' => array(
        'description' => 'Boolean indicating whether this should be public.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'size' => 'tiny',
        'default' => 0,
      ),
      'module' => $status_cols['module'],
      'status' => $status_cols['status'],
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'mt_code' => array('code'),
    ),
    'indexes' => array(
      'id' => array('id'),
      'code' => array('code'),
      'weight' => array('weight'),
      'live' => array('live'),
    ),
  );

  $schema['ug_whereisit_floors'] = array(
    'description' => 'Base table for floors.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for an area.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The title of this resource type, always treated as non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'code' => array(
        'description' => 'The internal code to use as an id for this floor.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'weight' => array(
        'description' => 'The order in which this resource should appear in lists.',
        'type' => 'int',
        'default' => 0,
      ),
      'image' => array(
        'description' => 'The normalization type of this material type.',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'image_width' => array(
        'description' => 'The width of the uploaded image.',
        'type' => 'int',
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'image_height' => array(
        'description' => 'The height of the uploaded image.',
        'type' => 'int',
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'module' => $status_cols['module'],
      'status' => $status_cols['status'],
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'floor_code' => array('code'),
    ),
    'indexes' => array(
      'id' => array('id'),
    ),
  );

  $schema['ug_whereisit_bounds'] = array(
    'description' => 'Base table for floors.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for an area.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'aid' => array(
        'description' => 'The ID of the area associated with this bouding box.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'x' => array(
        'description' => 'Horizontal position of bounding box.',
        'type' => 'float',
        'not null' => TRUE,
        'default' => 0,
      ),
      'y' => array(
        'description' => 'Vertical position of bouding box.',
        'type' => 'float',
        'not null' => TRUE,
        'default' => 0,
      ),
      'width' => array(
        'description' => 'The width of the bouding box.',
        'type' => 'float',
        'not null' => TRUE,
        'default' => 0,
      ),
      'height' => array(
        'description' => 'The height of the bouding box.',
        'type' => 'float',
        'not null' => TRUE,
        'default' => 0,
      ),
      'module' => $status_cols['module'],
      'status' => $status_cols['status'],
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'id' => array('id'),
      'area' => array('aid'),
    ),
    'foreign keys' => array(
      'aid' => array(
        'table' => 'ug_whereisit_areas',
        'columns' => array('aid' => 'id'),
      ),
    ),
  ); 

  return $schema;
}

function ug_whereisit_entity_exportable_schema_fields($module_col = 'module', $status_col = 'status') {
  return array(
    $status_col => array(
      'type' => 'int', 
      'not null' => TRUE,
      // Set the default to ENTITY_CUSTOM without using the constant as it is
      // not safe to use it at this point. 
      'default' => 0x01, 
      'size' => 'tiny', 
      'description' => 'The exportable status of the entity.',
    ), 
    $module_col => array(
      'description' => 'The name of the providing module if the entity has been defined in code.', 
      'type' => 'varchar', 
      'length' => 255, 
      'not null' => FALSE,
    ),
  );
}
