<?php

/**
 * Material type  class
 */
class MaterialType extends Entity {

  public function uri() {
    return array ('path' => 'admin/config/whereisit/types/type/' . $this->id);
  }

}
