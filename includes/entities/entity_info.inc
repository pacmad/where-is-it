<?php

function ug_whereisit_get_entity_info() {
  $entities = array();

  $entities['area'] = array(
    'label' => t('Area'),
    'plural label' => t('Areas'),
    'entity class' => 'Area',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'ug_whereisit_areas',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'label' => 'label',
      'name' => 'id',
    ),
    'menu wildcard' => '%ug_whereisit_area',
    'module' => 'ug_whereisit',
    'access callback' => 'ug_whereisit_access',
    'admin ui' => array(
      'path' => 'admin/config/whereisit/areas',
      'file' => 'includes/forms/area.admin.inc',
    ),
    // Use the default label() and uri() functions
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
  );

  $entities['material_type'] = array(
    'label' => t('Material Type'),
    'plural label' => t('Material Types'),
    'entity class' => 'MaterialType',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'ug_whereisit_material_types',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'label' => 'label',
      'name' => 'id',
    ),
    'menu wildcard' => '%ug_whereisit_material_type',
    'module' => 'ug_whereisit',
    'access callback' => 'ug_whereisit_access',
    'admin ui' => array(
      'path' => 'admin/config/whereisit/types',
      'file' => 'includes/forms/material_type.admin.inc',
    ),
    // Use the default label() and uri() functions
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
  );

  $entities['floor'] = array(
    'label' => t('Floor'),
    'plural label' => t('Floors'),
    'entity class' => 'Floor',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'ug_whereisit_floors',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'label' => 'label',
      'name' => 'id',
    ),
    'menu wildcard' => '%ug_whereisit_floor',
    'module' => 'ug_whereisit',
    'access callback' => 'ug_whereisit_access',
    'admin ui' => array(
      'path' => 'admin/config/whereisit/floors',
      'file' => 'includes/forms/floor.admin.inc',
    ),
    // Use the default label() and uri() functions
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
  );

  $entities['bound'] = array(
    'label' => t('Bound'),
    'plural label' => t('Bounds'),
    'entity class' => 'Bound',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'ug_whereisit_bounds',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'label' => 'label',
      'name' => 'id',
    ),
    // Use the default label() and uri() functions
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
  );
  
  return $entities;
}
