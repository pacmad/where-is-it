<?php 

function ug_whereisit_lookup_page() {
  module_load_include('inc', 'ug_whereisit', 'includes/callnumber');

  $params = drupal_get_query_parameters();

  //Doing a call number lookup.
  if( isset($params['cno']) && isset($params['mt']) ) {
    $cn = new CallNumber($params['cno']);
    $areas = ug_whereisit_get_call_areas($params['mt']);
    $area = ug_whereisit_area_search($areas, $cn);
    ug_whereisit_goto_area($area);
  }
 
  //Look up an area by floor and code. 
  if( isset($params['floor']) && isset($params['locn']) ) {
    $area = ug_whereisit_get_named_area($params['floor'], $params['locn']);
    ug_whereisit_goto_area($area);    
  }

  //Look up a floor, using only the floor code.
  if( isset($params['floor']) && !isset($params['locn']) ) {
    //TODO: Show a floor map.
    $floor = ug_whereisit_get_floor_by_code($params['floor']);
    ug_whereisit_goto_floor($floor);
  }

  //If we're not able to redirect properly, just stay here and display an error message directing users to get help.
  $page = array();
  $page['not-found'] = array(
    '#type' => 'markup',
    '#markup' => "Sorry, we can't find the item you're looking for. <a href='/ask-us/'>Ask us</a> to help you find your item.",
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  ); 

  return $page;
}
