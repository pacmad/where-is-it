<h2>View <?php echo $entity->label ?></h2>

<?php
  drupal_add_js(drupal_get_path('module', 'ug_whereisit') . '/js/map.js');
  $type = get_class($entity);
?>

<table id="where-entity-view">
  <tr>
    <th>Property</th>
    <th>Value</th>
  </tr>
  <tr>
    <td class="where-label">ID</td>
    <td class="where-value"><?php echo $entity->id ?></td>
  </tr>
  <tr>
    <td class="where-label">Label</td>
    <td class="where-value"><?php echo $entity->label ?></td>
  </tr>
  <tr>
    <td class="where-label">Code</td>
    <td class="where-value"><?php echo $entity->code ?></td>
  </tr>

<?php if($type == 'Area'): ?>
  <tr>
    <td class="where-label">Description</td>
    <td class="where-value"><?php echo $entity->description ?></td>
  </tr>
  <tr>
    <td class="where-label">Type</td>
    <td class="where-value"><?php echo $entity->type ?></td>
  </tr>
  <tr>
    <td class="where-label">Floor</td>
    <td class="where-value"><?php echo $entity->floor ?></td>
  </tr>

  <?php if($entity->type == 'call'): ?>
    <tr>
      <td class="where-label">Resource Type</td>
      <td class="where-value"><?php echo $entity->resource_type ?></td>
    </tr>
    <tr>
      <td class="where-label">Start</td>
      <td class="where-value"><?php echo $entity->start ?></td>
    </tr>
    <tr>
      <td class="where-label">End</td>
      <td class="where-value"><?php echo $entity->end ?></td>
    </tr>
  <?php endif; ?>
<?php endif;?>

<?php if($type == 'Floor'): ?>
  <tr>
    <td class="where-label">Weight</td>
    <td class="where-value"><?php echo $entity->weight ?></td>
  </tr>
<?php endif;?>

<?php if($type == 'MaterialType'): ?>
  <tr>
    <td class="where-label">Weight</td>
    <td class="where-value"><?php echo $entity->weight ?></td>
  </tr>
  <tr>
    <td class="where-label">Live</td>
    <td class="where-value"><?php echo $entity->live ?></td>
  </tr>
<?php endif;?>
</table>

<?php
/**
 * Render the map for areas.
 */
?>
<?php if($type == 'Area'): ?>
  <?php
  drupal_add_js(array('ug_whereisit' => array(
    'box_colour' => variable_get('ug_whereisit_box_colour', '000000'),
    'line_width' => variable_get('ug_whereisit_line_width', 3),
    'fill_transparency' => variable_get('ug_whereisit_fill_transparency', 0.7),
    'floor' => $entity->floor,
    'bounds' => json_encode($entity->bounds),
  )), array('type' => 'setting'));
  drupal_add_js(drupal_get_path('module', 'ug_whereisit') . '/js/area-public.js');
  drupal_add_css(drupal_get_path('module', 'ug_whereisit') . '/css/area.css');
  ?>
  <canvas id="map"></canvas>
<?php endif; ?>

<?php
/**
 * Render the floor image for floors.
 */
?>
<?php if($type == 'Floor'): ?>
  <?php drupal_add_css(drupal_get_path('module', 'ug_whereisit') . '/css/area.css'); ?>
  <img id="map" src="<?php echo file_create_url($entity->image); ?>" alt="Floor map"> 
<?php endif; ?>

