<?php
  drupal_add_css(drupal_get_path('module', 'ug_whereisit') . '/css/floor.css');
?>

<div id="map">

  <?php
    $image_path = file_create_url($element->image);
  ?>
  <img src="<?php echo $image_path?>" alt="<?php echo $element->label; ?>" />
</div>
<a id="where-back-link" class="ug-button" href="/where">Back to Where Is It</a>
