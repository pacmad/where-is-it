<?php

function ug_whereisit_main_page () {
  drupal_add_js(drupal_get_path('module', 'ug_whereisit') . '/js/where.js');
  drupal_add_css(drupal_get_path('module', 'ug_whereisit') . '/css/whereisit.css');

  $content = array();

  
  $content['left'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('where-column'),
    ),
  );

  //Call Numbers
  $content['left']['call_number'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'where-call-number',
    ),
  );

  $content['left']['call_number']['heading'] = array(
    '#type' => 'markup',
    '#markup' => t('By Call Number'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );

  $content['left']['call_number']['description'] = array(
    '#type' => 'markup',
    '#markup' => t('To locate a call number, choose the type of material  that you are looking for, and enter the call number in the text box.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
 
  module_load_include('inc', 'ug_whereisit', 'includes/forms/call_number');
  $content['left']['call_number']['search'] = drupal_get_form('ug_whereisit_call_number_form');

  $content['left']['call_number']['help'] = array(
    '#type' => 'markup',
    '#markup' => t('If you are unable to locate an item, please <a href="http://www.lib.uoguelph.ca/ask-us/">ask us</a>.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  //Locations
  $content['left']['location'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'where-location',
      'class' => array('where-column'),
    ),
  );

  $content['left']['location']['av']['heading'] = array(
    '#type' => 'markup',
    '#markup' => t('Audio Visual Collection'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );

  $content['left']['location']['av']['list'] = array(
    '#items' => array('2nd floor'),
    '#theme' => 'item_list',
  );

  $content['left']['location']['books']['heading'] = array(
    '#type' => 'markup',
    '#markup' => t('Books'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );

  $content['left']['location']['books']['list'] = array(
    '#items' => array(
      'A - K: 3rd floor',
      'L - P: 4th floor', 
      'Q - Z: 5th floor',
    ),
    '#theme' => 'item_list',
  );

  $content['left']['location']['Journals']['heading'] = array(
    '#type' => 'markup',
    '#markup' => t('Journals'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );

  $content['left']['location']['journals']['list'] = array(
    '#items' => array('2nd floor'),
    '#theme' => 'item_list',
  );

  $content['left']['location']['newspapers']['heading'] = array(
    '#type' => 'markup',
    '#markup' => t('Newspapers'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );

  $content['left']['location']['newspapers']['list'] = array(
    '#items' => array('2nd floor'),
    '#theme' => 'item_list',
  );

  //Named areas
  $content['named'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'where-named',
      'class' => array('where-column'),
    ),
  ); 

  module_load_include('inc', 'ug_whereisit', 'includes/forms/areas.named');
  module_load_include('inc', 'ug_whereisit', 'includes/forms/areas.floors');

  $content['named']['all-heading'] = array(
    '#type' => 'markup',
    '#markup' => t('By Room / Resource'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );

  $content['named']['all-description'] = array(
    '#type' => 'markup',
    '#markup' => t('To find facilities such as washrooms, computers, group study areas, etc., select the appropriate location from the menus below.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $content['named']['all'] = drupal_get_form('ug_whereisit_named_all_form');

  $content['named']['floor-heading'] = array(
    '#type' => 'markup',
    '#markup' => t('By Floor / Location'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );
  $content['named']['floors'] = drupal_get_form('ug_whereisit_named_floors_form');

  return $content;
}


