<?php 

function ug_whereisit_page() {
  $page = array();
 
  $links = array(
    l('General settings', 'admin/config/whereisit/config'),
    l('Manage Areas', 'admin/config/whereisit/areas'),
    l('Manage Named Areas', 'admin/config/whereisit/named-areas'),
    l('Manage Call Number Areas', 'admin/config/whereisit/call-number-areas'),
    l('Manage Floors', 'admin/config/whereisit/floors'),
    l('Manage Material Types', 'admin/config/whereisit/types'),
  );

  $page['list'] = array(
    '#theme' => 'item_list',
    '#items' => $links,
  );

  return $page;  
}
