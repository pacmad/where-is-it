<?php

/**
 * Page handlers for view actions on entities.
 */
function ug_whereisit_view_area($area) {
  $area = sanitize_entity($area);
  $content = array(
    '#theme'     => 'where-entity',
    '#entity'   => $area,
    '#view_mode' => $view_mode,
    '#language'  => LANGUAGE_NONE,
  );  

  return $content;
}

function ug_whereisit_view_floor($floor) {
  $floor = sanitize_entity($floor);
  $content = array(
    '#theme'     => 'where-entity',
    '#entity'   => $floor,
    '#view_mode' => $view_mode,
    '#language'  => LANGUAGE_NONE,
  );  

  return $content;
}

function ug_whereisit_view_material_type($type) {
  $type = sanitize_entity($type);
  $content = array(
    '#theme'     => 'where-entity',
    '#entity'   => $type,
    '#view_mode' => $view_mode,
    '#language'  => LANGUAGE_NONE,
  );  

  return $content;
}

/**
 * Sanitize the properties of an entity for presentation.
 **/
function sanitize_entity($entity) {
  foreach($entity as $key => $value) {
    if (is_string($value)) {
      $entity->$key = check_plain($value); 
    }
  }

  return $entity;
}
