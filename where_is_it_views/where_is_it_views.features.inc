<?php
/**
 * @file
 * where_is_it_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function where_is_it_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
