(function ($) {
  $(document).ready(function() {
    var map = new Map($('#map'));
    var floor_code = $('#edit-floor').val();
 
    map.updateMap(floor_code);
    addMapLinks();

    //Show the appropriate fieldset depending on the area type.
    showTypeFieldset();
    
    $('#edit-type').change(function() {
      showTypeFieldset();
    }); 

    //Changing the floor for the area means we have to update the base image of the map canvas.
    $('#edit-floor').change(function() {
      var floor_code = $('#edit-floor').val();
      map.updateMap(floor_code);
    });

    // DRAW BOX link click handlers which spawn the dialog box for drawing on the canvas.
    $('.map-dialog-link').click(function() {
      var box = $(this).closest('.box');
      var index = box.attr('index');
      
      $('#map').dialog({
        resizable: false,
        width: "auto",
        buttons: {
          'Cancel': function () {
            map.clear()
            $(this).dialog('close');
          },
          'Update': function () {
            updateForm(index, map);
            map.clear();
            $(this).dialog('close');
          }
        },
      });
    });
  
    /**
     * Code for tracking the coordinates of mouse clicks and drawing the box on the canvas.
     *
     * See map.js for more info.
     */
    var drawing = false;
    $('#map').click(function(e) {
      if(drawing == false) {
        drawing = true;
        map.setStartX(e.offsetX);
        map.setStartY(e.offsetY);
      }
      else {
        drawing = false;
        map.setEndX(e.offsetX);
        map.setEndY(e.offsetY);
      }
    });

    $('#map').mousemove(function(e) {
      if(drawing == true) {
        map.setEndX(e.offsetX);
        map.setEndY(e.offsetY);
        
        map.clear();
        map.drawBox();
      }
    });
  
  });

  /**
   * Show the appropriate type specific fieldset.
   **/
  function showTypeFieldset() {
    hideTypeFieldsets();
    var type = $('#edit-type').val();
    if( type == 'call' ) {
      $('#area-call-number').show();
    }
  }

  /**
   * Hide the type specific fielsets.
   **/
  function hideTypeFieldsets() { 
    $('#area-named').hide();
    $('#area-call-number').hide();
  }

  /**
   * Update the form elements with the values of the bounding box.
   */
  function updateForm(index, map) {

    var x_id = "#edit-x-" + index;
    var y_id = "#edit-y-" + index;
    var width_id = "#edit-width-" + index;
    var height_id = "#edit-height-" + index;

    var width = map.end_x - map.start_x;
    var height = map.end_y - map.start_y
    
    $(x_id).val(map.start_x);
    $(y_id).val(map.start_y);
    $(width_id).val(width);
    $(height_id).val(height);
  }

  /**
   * Go through and add links to launch the map dialog box.
   **/
  function addMapLinks() {
      $('.box').each(function() {
        var index = $(this).attr('index');
        var link = "<a class='map-dialog-link' href='javascript:void(0)'>(Draw Box " + index + ")</a>";
        var legend = $('legend span', this);
        $(legend).append(link);
      });
  }

}(jQuery));
