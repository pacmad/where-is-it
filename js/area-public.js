(function ($) {
  $(document).ready(function() {
    var map = new Map($('#map'));
    var floor_code = Drupal.settings.ug_whereisit.floor;
    var bounds = JSON.parse(Drupal.settings.ug_whereisit.bounds);

    //Update the base image of the canvas element with the proper floor.
    map.updateMap(floor_code, true);
     
    //Loop through and add the bounds to the map object.
    for (var key in bounds) {
      var bound = bounds[key];
      map.storeBound(bound);
    }

    //Run function when browser resizes
    $(window).resize( function () {
      map.fillContainer(); 
      map.drawBounds();
    });
  }); 

}(jQuery));
